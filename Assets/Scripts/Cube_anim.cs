﻿using UnityEngine;
using System.Collections;

public class Cube_anim : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		//Translate -z
		if(Input.GetKey(KeyCode.W)){
			transform.Translate (0f,0f,0.5f);
		}

		//Translate +x
		if(Input.GetKey(KeyCode.A)){
			transform.Translate (-0.5f,0f,0f);
		}

		//Translate -x
		if(Input.GetKey(KeyCode.D)){
			transform.Translate (0.5f,0f,0f);
		}
		//Translate +z
		if(Input.GetKey(KeyCode.S)){
			transform.Translate (0f,0f,-0.5f);
		}
		//Left arrow rotate
		if(Input.GetKey(KeyCode.LeftArrow)){
			transform.Rotate (0f,0.5f,0f);
		}
		//Right arrow rotate
		if(Input.GetKey(KeyCode.RightArrow)){
			transform.Rotate (0f,-0.5f,0f);
		}
		//Up arrow scale up
		if(Input.GetKey(KeyCode.UpArrow)){
			if (transform.localScale.x < 0.6f &&
			   transform.localScale.y < 0.6f &&
			   transform.localScale.z < 0.6f) {
					transform.localScale += new Vector3 (0.01f, 0.01f, 0.01f);
				}
		}
		//Down arrow scale down
		if(Input.GetKey(KeyCode.DownArrow)){
			if(transform.localScale.x>0.15f && 
				transform.localScale.y>0.15f && 
				transform.localScale.z>0.15f){
					transform.localScale += new Vector3 (-0.01f, -0.01f, -0.01f);
			}
		}

	}
}































